# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Alchemy puzzle-based programming game. (GOG edition)"
HOMEPAGE="https://www.zachtronics.com/opus-magnum/"
KEYWORDS="-* amd64 x86"
SLOT="0"

RDEPEND="
	app-crypt/libmd
	dev-libs/libbsd
	media-libs/alsa-lib
	media-libs/flac
	media-libs/libogg
	media-libs/libsdl2
	media-libs/libsndfile
	media-libs/libvorbis
	media-libs/opus
	media-sound/pulseaudio
	sys-apps/dbus
	sys-devel/gcc
	sys-libs/glibc
	sys-libs/zlib
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXcursor
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXinerama
	x11-libs/libXi
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libXxf86vm
	x11-libs/libX11"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Opus Magnum"
GOG_NATIVE_BIN_BASE="Lightning.bin"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/opus_magnum"

VERSION_PV="${PV#*.*.*.}"
TMP_DAY_PV="${PV%*.$VERSION_PV}"
DAY_PV="${TMP_DAY_PV#*.*.}"
TMP_MONTH_PV="${TMP_DAY_PV%*.$DAY_PV}"
MONTH_PV="${TMP_MONTH_PV#*.}"
YEAR_PV="${PV%*.*.*.$VERSION_PV}"
SRC_URI="opus_magnum_${MONTH_PV}_${DAY_PV}_${YEAR_PV}_${VERSION_PV}.sh"

src_install() {
	gog-native_src_install

	exeinto "/opt/${PN}/game"
	doexe "data/noarch/game/Lightning"

	if ! use "x86" ; then
		rm -r "${D}/opt/${PN}/game/lib"
	fi

	if ! use "amd64" ; then
		rm -r "${D}/opt/${PN}/game/lib64"
	fi
}

#######################################################################
#
# TODO:
#
#  * IUSE bundled-libs, v zakladu povolenej, kterej povoli kopirovani
#    knihoven ze zipu, pokud ho nekdo zakaze, pridaji se zavislosti
#    ze systemu (pozor na useflagy) a smazou se binarni verze knihoven
#    rozbaleny z archivu (libSDL2-2.0.so.0, ..)
