# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-dosbox

DESCRIPTION="2D platformer game from early 90s (GOG edition)"
HOMEPAGE="https://www.gog.com/en/game/prehistorik_12"
KEYWORDS="-* amd64 x86"
SLOT="0"

GOG_GAME_NAME="Prehistorik"
GOG_BUY_PAGE="https://www.gog.com/en/game/prehistorik_12"

SRC_URI="gog_prehistorik_${PV}.sh"
