# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit retrogames

# abandonware
LICENSE="all-rights-reserved"
DESCRIPTION="Grand Prix Circuit is a motor racing game."
HOMEPAGE="https://www.retrogames.cz/play_413-DOS.php"
SLOT="0"
SRC_URI="http://www.retrogames.cz/dos/gpc.zip -> ${P}.zip"

RETROGAMES_NAME="Grand Prix Circuit"
RETROGAMES_IMG_NAME="gpc.img"
RETROGAMES_IMG_SIZE="512,8,2,384"
RETROGAMES_COMMAND="gpega"
