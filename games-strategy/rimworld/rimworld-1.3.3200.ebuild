# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

DESCRIPTION="A sci-fi colony sim driven by an intelligent AI storyteller. Kickstarter version."
HOMEPAGE="https://rimworldgame.com"

inherit desktop

LICENSE="rimworld-EULA"
SLOT="0"
KEYWORDS="amd64 x86"
RESTRICT="fetch bindist strip"

# ldd /opt/rimworld/RimWorldLinux | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
# ldd /opt/rimworld/UnityPlayer.so | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}"

SRC_URI="RimWorld${PV//./-}Linux.tar.gz"

S="${WORKDIR}/RimWorld${PV//./-}Linux"

pkg_nofetch() {
	einfo "Please buy and download ${A} from:"
	einfo "   https://rimworldgame.com"
	einfo "and move it to your DISTDIR"
}

src_install() {
	insinto "/opt/${PN}"
	doins -r \
		"Data" \
		"Mods" \
		"RimWorldLinux_Data" \
		"LinuxPlayer_s.debug" \
		"ScenarioPreview.jpg" \
		"UnityPlayer.so" \
		"UnityPlayer_s.debug"

	exeinto "/opt/${PN}"
	doexe "RimWorldLinux"
	# doexe "start_RimWorld.sh"
	# doexe "start_RimWorld_openglfix.sh"

	dodoc "EULA.txt"
	dodoc "Licenses.txt"
	dodoc "ModUpdating.txt"
	dodoc "Notes for Linux Users.txt"
	dodoc "Readme.txt"

	domenu "${FILESDIR}/RimWorld.desktop"
}
