# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit xdg-utils

# abandonware
LICENSE="all-rights-reserved"
DESCRIPTION="Action-platform game in which players travel through one hundred different stages, blowing and bursting bubbles."
HOMEPAGE="https://www.classicgames.me/bubble-bobble-on-msdos.html"
SLOT="0"
SRC_URI="
	https://downloaddosgames.classicgames.me/d.php?game=71c751f42568b976c0ee824099ea4563ad50 -> ${P}.zip
	https://www.retrogames.cz/manualy/NES/NintendoNESBubbleBobble1.pdf"

KEYWORDS="~* amd64 x86"
RESTRICT="mirror test"
BDEPEND="app-arch/unzip"
RDEPEND="games-emulation/dosbox"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

NAME="Bubble Bobble"
COMMAND="BUBBLE.EXE"

S="${WORKDIR}/BubbBobb"

src_install() {
	DESTINATION="/opt/${PN}"

	# files
	insinto "${DESTINATION}"
	doins *

	# docs
	dodoc "${DISTDIR}/NintendoNESBubbleBobble1.pdf"

	# desktop
	DESKTOP="${T}/${PN}.desktop"
	cat <<-EOF > "${DESKTOP}" || die
	[Desktop Entry]
	Name=${NAME}
	Type=Application
	Comment=${DESCRIPTION}
	Exec=dosbox "${DESTINATION}/${COMMAND}"
	Icon=/usr/share/pixmaps/dosbox.ico
	Categories=Game;
	EOF
	(
		insopts -m "0644"
		insinto "/usr/share/applications"
		doins "${DESKTOP}"
	) || die "installing desktop file failed"
}

retrogames_pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

retrogames_pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
