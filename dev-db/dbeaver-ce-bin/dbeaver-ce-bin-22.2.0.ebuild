# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop xdg-utils

MY_PN="${PN%-bin}"

DESCRIPTION="Universal database tool for developers and database administrators"
HOMEPAGE="https://dbeaver.io/about/"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
LICENSE="Apache-2.0 BSD EPL-1.0"

IUSE="+system-jre"

SRC_URI="
	system-jre? ( https://dbeaver.io/files/${PV}/${MY_PN}-${PV}-linux.gtk.x86_64-nojdk.tar.gz )
	!system-jre? ( https://dbeaver.io/files/${PV}/${MY_PN}-${PV}-linux.gtk.x86_64.tar.gz )"

RDEPEND="system-jre? ( >=virtual/jre-17:* )"
S="${WORKDIR}/dbeaver"

src_install() {
	insinto "/usr/share/${MY_PN}"
	doins -r configuration features p2 plugins
	if ! use system-jre; then
		doins -r jre
	fi
	doins .eclipseproduct dbeaver.ini dbeaver.png icon.xpm

	if ! use system-jre; then
		exeinto "/usr/share/${MY_PN}/jre/bin/"
		doexe jre/bin/java
	fi

	exeinto "/usr/share/${MY_PN}"
	doexe dbeaver

	# desktop
	domenu dbeaver-ce.desktop
}

pkg_postinst() {
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
