# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: gog-native.eclass
# @MAINTAINER:
# Martin Kužel <martin@kuzelovi.eu>
# @SUPPORTED_EAPIS: 7 8
# @BLURB: support for GOG.com linux native games installer


#
# How to get close sorce game dependencies:
#   ldd "/opt/${PN}/game/${GOG_NATIVE_BIN_BASE}" | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
# some games depend on shipped libraries:
#   ldd "/opt/${PN}/game/UnityPlayer.so | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
#


case ${EAPI} in
	7|8) ;;
	*) die "${ECLASS}: EAPI ${EAPI:-0} not supported" ;;
esac


inherit desktop xdg-utils


LICENSE="gog-EULA"
RESTRICT="bindist fetch mirror test"
BDEPEND="app-arch/unzip"

EXPORT_FUNCTIONS pkg_nofetch src_unpack src_install pkg_postinst pkg_postrm


gog-native_pkg_nofetch() {
	einfo "Please buy and download:"
	for GOG_NATIVE_DIST_FILE in ${A} ; do
		einfo "  ${GOG_NATIVE_DIST_FILE}"
	done
	einfo "from:"
	einfo "  ${GOG_NATIVE_BUY_PAGE}"
	einfo "and move it to your DISTDIR"
}


gog-native_src_unpack() {
	for GOG_NATIVE_DIST_FILE in ${A} ; do
		echo ">>> Unpacking ${GOG_NATIVE_DIST_FILE} to ${S}"
		unzip -o "${DISTDIR}/${GOG_NATIVE_DIST_FILE}" -d "${S}" > /dev/null 2>&1 || true
	done
}


gog-native_src_install() {
	GOG_NATIVE_DESTINATION="/opt/${PN}"

	# files
	insinto "${GOG_NATIVE_DESTINATION}"
	doins -r "data/noarch/game" "data/noarch/gameinfo"

	insinto "${GOG_NATIVE_DESTINATION}/support"
	doins "data/noarch/support/gog"*

	# docs
	dodoc -r data/noarch/docs/*

	# icon
	newicon -s 256 "data/noarch/support/icon.png" "${PN}.png"

	# start.sh
	exeinto "${GOG_NATIVE_DESTINATION}"
	doexe "data/noarch/start.sh"

	# runnable
	exeinto "${GOG_NATIVE_DESTINATION}/game"

	if [ -f "data/noarch/game/${GOG_NATIVE_BIN_BASE}" ]; then
		doexe "data/noarch/game/${GOG_NATIVE_BIN_BASE}"
	elif [ -f "data/noarch/game/${GOG_NATIVE_BIN_BASE}".x86$(usex amd64 _64 "") ]; then
		doexe "data/noarch/game/${GOG_NATIVE_BIN_BASE}".x86$(usex amd64 _64 "")
	else
		die "Package ${P} cannot be installed by ${ECLASS}"
	fi

	# desktop
	make_desktop_entry "${GOG_NATIVE_DESTINATION}/start.sh" "${GOG_NATIVE_GAME_NAME}" "${PN}"
}


gog-native_pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}


gog-native_pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
