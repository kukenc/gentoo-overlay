# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Slain! is a platform game, shares similarities to the Castlevania series (GOG edition)"
HOMEPAGE="https://www.digerati.games/game/slain/"
KEYWORDS="-* amd64"
SLOT="0"
IUSE="fix-keyboard"

RDEPEND="
	app-crypt/libmd
	dev-libs/libbsd
	media-libs/libglvnd
	sys-devel/gcc
	sys-libs/glibc
	fix-keyboard? (
		sys-apps/gawk
		x11-apps/setxkbmap )
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXcursor
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libX11"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Slain: Back From Hell"
GOG_NATIVE_BIN_BASE="Slain"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/slain"

SRC_URI="gog_slain_back_from_hell_${PV}.sh"

src_prepare() {
	# for default see https://devmanual.gentoo.org/ebuild-writing/functions/src_prepare/index.html
	if [[ $(declare -p PATCHES 2>/dev/null) == "declare -a"* ]]; then
		[[ -n ${PATCHES[@]} ]] && eapply "${PATCHES[@]}"
	else
		[[ -n ${PATCHES} ]] && eapply ${PATCHES}
	fi

	use "fix-keyboard" && eapply "${FILESDIR}/${PN}-001-fix-non-us-keyboard.patch"

	eapply_user
}
