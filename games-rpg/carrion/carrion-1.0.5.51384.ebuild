# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Reverse horror game in which you assume the role of an amorphous creature of unknown origin. (GOG edition)"
HOMEPAGE="https://www.carrion.game"
KEYWORDS="-* amd64"
SLOT="0"

RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Carrion"
GOG_NATIVE_BIN_BASE="Carrion"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/carrion"

SRC_URI="carrion_1_0_5_gog_e488b7a83da711ecb28a342eb79e7a9f_51384.sh"

#######################################################################
#
# TODO:
#
#  * IUSE bundled-libs, v zakladu povolenej, kterej povoli kopirovani
#    knihoven ze zipu, pokud ho nekdo zakaze, pridaji se zavislosti
#    ze systemu (pozor na useflagy) a smazou se binarni verze knihoven
#    rozbaleny z archivu (libSDL2-2.0.so.0, ..)
