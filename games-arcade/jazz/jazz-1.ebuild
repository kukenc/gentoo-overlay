# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit retrogames

LICENSE="all-rights-reserved"
DESCRIPTION="This little bunny with a very big gun is on a mission to save the beloved rabbit princess."
HOMEPAGE="https://www.retrogames.cz/play_652-DOS.php"
SLOT="0"
SRC_URI="http://www.retrogames.cz/dos/jazz.zip -> ${P}.zip"

RETROGAMES_NAME="Jazz Jackrabbit (DOS)"
RETROGAMES_IMG_NAME="jazz.img"
RETROGAMES_IMG_SIZE="512,8,2,384"
RETROGAMES_COMMAND="JAZZ.EXE"
