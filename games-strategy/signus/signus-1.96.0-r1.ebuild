# Copyright 2021 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit autotools desktop

DESCRIPTION="Turn-based sci-fi strategy wargame"
HOMEPAGE="https://github.com/signus-game/signus"

if [[ ${PV} == 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}"
	SRC_URI=""
	EGIT_SUBMODULES=()
else
	SRC_URI="${HOMEPAGE}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
fi

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="debug"
RESTRICT="debug? ( strip )"

RDEPEND="
	media-libs/libsdl2:=
	media-libs/sdl2-image:=[jpeg]
	media-libs/sdl2-mixer:=[midi,mod,vorbis]
	media-libs/sdl2-ttf:="
DEPEND="${RDEPEND}
	media-libs/libpng"

PATCHES=(
	"${FILESDIR}/${P}-001-fix-namespace-issue-in-makedat-util.patch"
	"${FILESDIR}/${P}-002-remove-register-keyword.patch"
	"${FILESDIR}/${P}-003-fix-font-loading-sanity-check.patch"
	"${FILESDIR}/${P}-005-add-libpng-dependency-to-configureac.patch"
	"${FILESDIR}/${P}-007-add-support-for-libSDL2.0.5-and-newer.patch"
	"${FILESDIR}/${P}-008-add-fullscreen-option.patch"
	"${FILESDIR}/${P}-009-make-game-changing-bugfixes-optional.patch"
	"${FILESDIR}/${P}-010-fix-checkbox-width.patch"
	"${FILESDIR}/${P}-011-cleanup-RunSingnus-method.patch"
	"${FILESDIR}/${P}-012-fix-screen-size-after-starting-from-crashguard-save.patch"
	"${FILESDIR}/${P}-013-cleanup-HandleIcons-method.patch"
	"${FILESDIR}/${P}-014-redesign-post-crash-game-start.patch"
	"${FILESDIR}/${P}-015-add-optional-low-fuel-warning-for-aircrafts.patch"
	"${FILESDIR}/${P}-017-fix-radar-destruction-check-in-mission-7-AI.patch"
	"${FILESDIR}/${P}-900-kukenc-update-for-autoconf-2.71.patch"
	"${FILESDIR}/${P}-901-kukenc-explicit-use-of-gwarf-with-debug.patch"
)

src_prepare() {
	default
	cd signus
	eaclocal
	eautoconf
	eautoheader
	eautomake
	cd ../signus-data
	eaclocal
	eautoconf
	eautomake
	cd ..
}

src_configure() {
	cd signus
	econf $(use_enable debug)
	cd ../signus-data
	econf
	cd ..
}

src_compile() {
	cd signus
	emake
	cd ../signus-data
	emake
	cd ..
}

src_install() {
	cd signus
	emake DESTDIR="${D}" install
	dodoc AUTHORS
	cd ../signus-data
	emake DESTDIR="${D}" install
	cd ..
	domenu "${FILESDIR}/signus.desktop"
}
