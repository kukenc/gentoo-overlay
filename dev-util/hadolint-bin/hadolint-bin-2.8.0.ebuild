# Copyright 2021 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

MY_PN=${PN/-bin/}

DESCRIPTION="Dockerfile analysis tool (binary package)"
HOMEPAGE="https://github.com/hadolint/hadolint"
SRC_URI="https://github.com/${MY_PN}/${MY_PN}/releases/download/v${PV}/${MY_PN}-Linux-x86_64 -> ${P}.x86_64"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64"

DEPEND="!dev-util/hadolint"
RDEPEND="${DEPEND}"

QA_PREBUILT="usr/bin/hadolint"
QA_PRESTRIPPED="usr/bin/hadolint"

S="${DISTDIR}"

src_install() {
	newbin ${P}.x86_64 hadolint
}
