# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit retrogames

# freeware
LICENSE="all-rights-reserved"
DESCRIPTION="Freeware 90's clone of Snake"
HOMEPAGE="https://www.retrogames.cz/play_487-DOS.php"
SLOT="0"
SRC_URI="https://www.retrogames.cz/dos/vlak.zip -> ${P}.zip"

RETROGAMES_NAME="Vlak"
RETROGAMES_IMG_NAME="vlak.img"
RETROGAMES_IMG_SIZE="512,8,2,384"
RETROGAMES_COMMAND="vlak"
