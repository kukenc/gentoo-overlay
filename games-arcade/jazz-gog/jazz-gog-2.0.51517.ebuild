# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-dosbox

DESCRIPTION="This little bunny - with a very big gun - is on a mission to save the beloved rabbit princess (GOG edition)"
HOMEPAGE="https://www.gog.com/game/jazz_jackrabbit_collection"
KEYWORDS="-* amd64 x86"
SLOT="0"

GOG_GAME_NAME="Jazz Jackrabbit (GOG)"
GOG_BUY_PAGE="https://www.gog.com/game/jazz_jackrabbit_collection"

SRC_URI="jazz_jackrabbit_collection_${PV//./_}.sh"

# Game archive has different structure from eg. prehistorik
src_install() {
	GOG_DESTINATION="/opt/${PN}"

	# files
	insinto "${GOG_DESTINATION}"
	doins -r "data/noarch/game/data" "data/noarch/gameinfo" "data/noarch/game/dosbox"*".conf"

	insinto "${GOG_DESTINATION}/support"
	doins "data/noarch/support/gog"*

	# docs
	dodoc -r data/noarch/docs/*
	# QA Notice: One or more compressed files were found in docompress-ed
	# directories. Please fix the ebuild not to install compressed files
	# (manpages, documentation) when automatic compression is used:
	rm "${D}/usr/share/doc/${P}/dosbox-"*

	# icon
	newicon -s 256 "data/noarch/support/icon.png" "${PN}.png"

	# start.sh
	exeinto "${GOG_DESTINATION}"
	doexe "data/noarch/start.sh"

	# desktop
	make_desktop_entry "${GOG_DESTINATION}/start.sh" "${GOG_GAME_NAME}" "${PN}"
}
