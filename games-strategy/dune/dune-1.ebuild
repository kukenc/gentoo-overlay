# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit retrogames

# abandonware
LICENSE="all-rights-reserved"
DESCRIPTION="A real-time strategy Dune video game."
HOMEPAGE="https://www.retrogames.cz/play_504-DOS.php"
SLOT="1"
SRC_URI="
	http://www.retrogames.cz/dos/dune.zip -> ${P}.zip
	https://www.retrogames.cz/manualy/DOS/Dune_-_Manual_-_PC.pdf"

RETROGAMES_NAME="Dune"
RETROGAMES_IMG_NAME="dune.img"
RETROGAMES_IMG_SIZE="512,8,2,384"
RETROGAMES_COMMAND="dune"
RETROGAMES_MAN="Dune_-_Manual_-_PC.pdf"
