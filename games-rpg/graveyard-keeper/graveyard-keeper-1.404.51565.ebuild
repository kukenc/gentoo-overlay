# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="The most inaccurate medieval cemetery management sim of all time (GOG edition)"
HOMEPAGE="https://www.graveyardkeeper.com"
KEYWORDS="-* amd64"
SLOT="0"
#IUSE="better-save-soul game-of-crone stranger-sins cz"
IUSE="better-save-soul game-of-crone stranger-sins"

RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Graveyard Keeper"
GOG_NATIVE_BIN_BASE="Graveyard Keeper"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/graveyard_keeper"

SRC_URI="
	graveyard_keeper_${PV//./_}.sh
	better-save-soul? ( graveyard_keeper_better_save_soul_${PV//./_}.sh )
	game-of-crone? ( graveyard_keeper_game_of_crone_${PV//./_}.sh )
	stranger-sins? ( graveyard_keeper_stranger_sins_${PV//./_}.sh )"
	# cz? ( resources_CZ_GOG_1.404.zip )"

# src_install() {
# 	gog-native_src_install
# 	if use "cz" ; then
# 		insinto "${GOG_NATIVE_DESTINATION}/game/Graveyard Keeper_Data"
# 		doins resources.assets
# 	fi
# }
