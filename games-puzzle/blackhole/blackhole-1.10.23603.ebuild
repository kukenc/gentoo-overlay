# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Blackhole is a puzzle-platform game (GOG edition)"
HOMEPAGE="https://www.blackhole-game.com/en/about"
KEYWORDS="-* amd64 x86"
SLOT="0"
IUSE="complete-edition"

RDEPEND="
	app-crypt/libmd
	dev-libs/gmp
	dev-libs/libbsd
	dev-libs/libtasn1
	dev-libs/libunistring
	dev-libs/nettle
	dev-libs/openssl-compat:1.0.0
	dev-libs/openssl
	media-libs/glu
	media-libs/libglvnd
	media-libs/openal
	net-dns/libidn2
	net-libs/gnutls
	net-libs/libssh2
	net-libs/nghttp2
	net-misc/curl
	sys-devel/gcc
	sys-libs/glibc
	sys-libs/zlib
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libXxf86vm
	x11-libs/libX11"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Blackhole"
GOG_NATIVE_BIN_BASE="BLACKHOLE"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/blackhole"

SRC_URI="
	blackhole_${PV//./_}.sh
	complete-edition? ( blackhole_complete_edition_upgrade_${PV//./_}.sh )"
