# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="The classic snake game for the terminal"
HOMEPAGE="https://github.com/AngelJumbo/sssnake"
SRC_URI="https://github.com/AngelJumbo/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT-with-advertising"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="-doc"

# ldd /usr/bin/sssnake | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
RDEPEND="
	sys-libs/glibc
	sys-libs/ncurses:0="
BDEPEND="doc? ( virtual/pandoc )"
DEPEND="
	${BDEPEND}
	${RDEPEND}"

src_prepare() {
	default
	# install to gentoo default location
	sed -i 's,^PREFIX.*,PREFIX = /usr,g' makefile
}

src_compile() {
	# overload makefile for compile
	cc -w $(ncursesw6-config --cflags --libs) main.c autopilot.c xymap.c essentials.c snake.c -lncursesw -o sssnake
	use doc && pandoc docs/sssnake.1.md -s -t man -o docs/sssnake.1
}
