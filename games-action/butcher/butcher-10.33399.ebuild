# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="BUTCHER is a fast-paced 2D shooter game and a blood-soaked love letter to the early 90s (GOG edition)"
HOMEPAGE="https://butcher.thd.vg"
KEYWORDS="-* amd64 x86"
SLOT="0"

RDEPEND="
	app-crypt/libmd
	dev-libs/libbsd
	media-libs/libglvnd
	sys-devel/gcc
	sys-libs/glibc
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXcursor
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libX11"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Butcher"
GOG_NATIVE_BIN_BASE="Butcher"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/butcher"

SRC_URI="butcher_w_i_m_p_gog_${PV//./_}.sh"

src_install() {
	gog-native_src_install

	if ! use "x86" ; then
		# QA_EXECSTACK="
		# 	opt/epistory/game/Butcher.x86
		# 	opt/epistory/game/Butcher_Data/Mono/x86/libmono.so
		# 	opt/epistory/game/Butcher_Data/Plugins/x86/libfmod.so"

		rm -r "${D}/opt/${PN}/game/Butcher.x86"
		rm -r "${D}/opt/${PN}/game/Butcher_Data/Mono/x86"
		rm -r "${D}/opt/${PN}/game/Butcher_Data/Plugins/x86"
	fi

	if ! use "amd64" ; then
		# QA_EXECSTACK="
		# 	opt/butcher/game/Butcher.x86_64
		# 	opt/butcher/game/Butcher_Data/Plugins/x86_64"
		rm -r "${D}/opt/${PN}/game/Butcher.x86_64"
		rm -r "${D}/opt/${PN}/game/Butcher_Data/Plugins/x86_64"
	fi
}
