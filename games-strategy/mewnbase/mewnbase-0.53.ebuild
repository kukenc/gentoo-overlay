# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

DESCRIPTION="A Space-Cat survival and base-building game. (itch.io edition)."
HOMEPAGE="https://cairn4.itch.io/mewnbase"

inherit desktop xdg-utils

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="-* amd64 x86"
RESTRICT="fetch bindist strip"

# ldd /opt/mewnbase/MewnBase | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
# ldd /opt/lennas-inception/jre/lib/amd64/jli/libjli.so | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
RDEPEND="
	sys-libs/glibc
	sys-libs/zlib"
DEPEND="
	${RDEPEND}"

SRC_URI="mewnbase-linux_${PV}.zip"

S="${WORKDIR}/"

DOCS="readme.txt"

pkg_nofetch() {
	einfo "Please buy and download ${A} from:"
	einfo "   https://cairn4.itch.io/mewnbase"
	einfo "and move it to your DISTDIR"
}

src_install() {
	insinto "/opt/${PN}"
	doins -r data jre
	doins desktop-1.0.jar config.json "${FILESDIR}/settings.json"
	fperms 666 "/opt/${PN}/settings.json"

	keepdir "/opt/${PN}/saves"
	fperms 777 "/opt/${PN}/saves"

	einstalldocs

	exeinto "/opt/${PN}"
	doexe MewnBase

	# desktop
	make_desktop_entry "/opt/${PN}/MewnBase" "MewnBase" "${PN}"
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

gog-native_pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
