# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Hike, climb, and soar through the peaceful mountainside landscapes of Hawk Peak Provincial Park (GOG edition)"
HOMEPAGE="https://ashorthike.com"
KEYWORDS="-* amd64 x86"
SLOT="0"

RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"

DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="A Short Hike"
GOG_NATIVE_BIN_BASE="AShortHike"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/en/game/a_short_hike"

BUILD_PV="${PV#*.*.*.}"
TMP_V_PV="${PV%*.$BUILD_PV}"
V_PV="${TMP_V_PV#*.*.}"
TMP_MINOR_PV="${TMP_V_PV%*.$V_PV}"
MINOR_PV="${TMP_MINOR_PV#*.}"
MAJOR_PV="${PV%*.*.*.$BUILD_PV}"
SRC_URI="a_short_hike_${MAJOR_PV}_${MINOR_PV}_v${V_PV}_${BUILD_PV}.sh"
