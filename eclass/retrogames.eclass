# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: retrogames.eclass
# @MAINTAINER:
# Martin Kužel <martin@kuzelovi.eu>
# @SUPPORTED_EAPIS: 7 8
# @BLURB: support for GOG.com linux games over dosbox installer


case ${EAPI} in
	7|8) ;;
	*) die "${ECLASS}: EAPI ${EAPI:-0} not supported" ;;
esac

inherit xdg-utils

KEYWORDS="amd64 arm ppc ppc64 x86"
RESTRICT="mirror test"
BDEPEND="app-arch/unzip"
RDEPEND="games-emulation/dosbox"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

EXPORT_FUNCTIONS src_unpack src_install pkg_postinst pkg_postrm


retrogames_src_unpack() {
	for RETROGAMES_DIST_FILE in ${A} ; do
		if [[ "${RETROGAMES_DIST_FILE##*.}" == "zip" ]]; then
			echo ">>> Unpacking ${RETROGAMES_DIST_FILE} to ${S}"
			unzip "${DISTDIR}/${RETROGAMES_DIST_FILE}" -d "${S}" || die "unzip failed"
		fi
	done
}


retrogames_src_install() {
	if [[ "${SLOT}" -eq "0" ]]; then
		RETROGAMES_DESTINATION="/opt/${PN}"
	else
		RETROGAMES_DESTINATION="/opt/${PN}-${SLOT}"
	fi

	# files
	(
		# wrap the env here so that the 'insinto' call
		# doesn't corrupt the env of the caller
		insopts -m "0666"
		insinto "${RETROGAMES_DESTINATION}"
		doins "${RETROGAMES_IMG_NAME}"
	) || die "installing img file failed"

	# docs
	if [ -n "${RETROGAMES_MAN}" ]; then
		dodoc "${DISTDIR}/${RETROGAMES_MAN}"
	fi

	# desktop
	if [[ "${SLOT}" -eq "0" ]]; then
		DESKTOP="${T}/${PN}.desktop"
	else
		DESKTOP="${T}/${PN}-${SLOT}.desktop"
	fi
    cat <<-EOF > "${DESKTOP}" || die
	[Desktop Entry]
	Name=${RETROGAMES_NAME}
	Type=Application
	Comment=${DESCRIPTION}
	Exec=dosbox -c "mount c ${RETROGAMES_DESTINATION}" -c "imgmount d ${RETROGAMES_DESTINATION}/${RETROGAMES_IMG_NAME} -size ${RETROGAMES_IMG_SIZE}" -c "d:" -c "${RETROGAMES_COMMAND}" -c "exit"
	Icon=/usr/share/pixmaps/dosbox.ico
	Categories=Game;
	EOF
	(
		# wrap the env here so that the 'insinto' call
		# doesn't corrupt the env of the caller
		insopts -m "0644"
		insinto "/usr/share/applications"
		doins "${DESKTOP}"
	) || die "installing desktop file failed"
}


retrogames_pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}


retrogames_pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
