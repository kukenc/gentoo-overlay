# Copyright 2021 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit unpacker

DESCRIPTION="A command-line tool to quickly analyze which IPs have open ports/vulnerabilities."
HOMEPAGE="https://gitlab.com/shodan-public/nrich"
SRC_URI="https://gitlab.com/api/v4/projects/33695681/packages/generic/nrich/${PV}/nrich_${PV}_amd64.deb -> ${P}.deb"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64"
S="${WORKDIR}"

RDEPEND="
	dev-libs/openssl:0
	sys-devel/gcc
	sys-libs/glibc
	!net-analyzer/nrich"

src_install() {
	dobin usr/bin/nrich
	dodoc usr/share/doc/nrich/*
}
