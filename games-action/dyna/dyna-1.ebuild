# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit retrogames

# abandonware
LICENSE="all-rights-reserved"
DESCRIPTION="Belonging to the Bomberman franchise, it is a greatly expanded re-imagining of the first game in the series."
HOMEPAGE="https://www.retrogames.cz/play_455-DOS.php"
SLOT="0"
SRC_URI="https://www.retrogames.cz/dos/dyna.zip -> ${P}.zip"

RETROGAMES_NAME="Dyna Blaster (Bomberman)"
RETROGAMES_IMG_NAME="dyna.img"
RETROGAMES_IMG_SIZE="512,8,2,384"
RETROGAMES_COMMAND="dyna"
