# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Classically styled 2D action adventure across a vast interconnected world (GOG edition)"
HOMEPAGE="https://www.hollowknight.com"
KEYWORDS="-* amd64"
SLOT="0"
IUSE="gods-nightmares"

RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Hollow Knight"
GOG_NATIVE_BIN_BASE="Hollow Knight"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/hollow_knight"

SRC_URI="
	hollow_knight_${PV//./_}.sh
	gods-nightmares? ( hollow_knight_gods_nightmares_${PV//./_}.sh )"
