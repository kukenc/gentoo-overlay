# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit retrogames

# abandonware
LICENSE="all-rights-reserved"
DESCRIPTION="Prince of Persia is a retro fantasy platform game."
HOMEPAGE="https://www.retrogames.cz/play_102-DOS.php"
SLOT="0"
SRC_URI="
	https://www.retrogames.cz/dos/prince.zip -> ${P}.zip
	https://www.retrogames.cz/manualy/DOS/Prince_of_Persia_Manual.pdf"

RETROGAMES_NAME="Prince of Persia"
RETROGAMES_IMG_NAME="prince.img"
RETROGAMES_IMG_SIZE="512,8,2,384"
RETROGAMES_COMMAND="START.BAT"
RETROGAMES_MAN="Prince_of_Persia_Manual.pdf"
