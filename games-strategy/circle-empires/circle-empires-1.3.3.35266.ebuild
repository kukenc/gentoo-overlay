# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Fast-paced real-time strategic gameplay with large-scale battles (GOG edition)"
HOMEPAGE="http://circleempires.com"
KEYWORDS="-* amd64 x86"
SLOT="0"
IUSE="apex-monsters"

RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Circle Empires"
GOG_NATIVE_BIN_BASE="circle_empires"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/circle_empires"

SRC_URI="
	circle_empires_${PV//./_}.sh
	apex-monsters? ( circle_empires_apex_monsters_${PV//./_}.sh )"

src_install() {
	gog-native_src_install

	if ! use "x86" ; then
		# QA_TEXTRELS="opt/circle-empires/circle_empires_Data/Plugins/x86/ScreenSelector.so"
		rm -r "${D}/opt/${PN}/game/circle_empires_Data/Plugins/x86"
		# QA_EXECSTACK="opt/circle-empires/circle_empires_Data/Mono/x86/libmono.so"
		rm -r "${D}/opt/${PN}/game/circle_empires_Data/Mono/x86"
	fi
}
