# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Homage to classics from golden era of strategy games, with modern spin on controls and user experience (GOG edition)"
HOMEPAGE="https://www.gog.com/game/loria"
LICENSE="all-rights-reserved"
KEYWORDS="-* amd64"
SLOT="0"

RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Loria"
GOG_NATIVE_BIN_BASE="Loria"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/loria"

SRC_URI="loria_${PV//./_}.sh"
