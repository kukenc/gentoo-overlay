# Maintainer: Xoores <me@xoores.cz>

EAPI=8

#inherit eutils

DESCRIPTION="Tool to monitor network traffic based on processes"
HOMEPAGE="https://github.com/berghetti/netproc"

if [[ ${PV} == 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}"
	SRC_URI=""
	EGIT_SUBMODULES=()
else
	SRC_URI="${HOMEPAGE}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
fi

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ˚~x86"
IUSE=""

DEPEND="
	sys-libs/glibc
	sys-libs/ncurses
	"

RDEPEND="${DEPEND}"

src_install()
{
	dosbin "bin/${PN}"
	doman "doc/${PN}.8"
}
