# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Drill for oil like it's 1899! (GOG edition)"
HOMEPAGE="https://gamious.com/portfolio/turmoil/"
KEYWORDS="-* amd64"
SLOT="0"
IUSE="heat-is-on"

RDEPEND="
	app-crypt/libmd
	dev-libs/libbsd
	dev-libs/openssl-compat:1.0.0
	media-libs/glu
	media-libs/libglvnd
	media-libs/openal
	sys-devel/gcc
	sys-libs/glibc
	sys-libs/zlib
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libXxf86vm
	x11-libs/libX11"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Turmoil"
GOG_NATIVE_BIN_BASE="runner"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/en/game/turmoil"

SRC_URI="
	turmoil_en_${PV//./_}.sh
	heat-is-on? ( turmoil_the_heat_is_on_dlc_en_2_0_9_20678.sh )"
