# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Puzzle game for nerds. (GOG edition)"
HOMEPAGE="https://tomorrowcorporation.com/humanresourcemachine"
KEYWORDS="-* amd64 x86"
SLOT="0"

RDEPEND="
	app-crypt/libmd
	dev-libs/libbsd
	media-libs/alsa-lib
	media-libs/flac
	media-libs/libglvnd
	media-libs/libogg
	media-libs/libsndfile
	media-libs/libvorbis
	media-libs/openal
	media-libs/opus
	media-sound/pulseaudio
	sys-apps/dbus
	sys-devel/gcc
	sys-libs/glibc
	sys-libs/zlib
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXcursor
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXinerama
	x11-libs/libXi
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libXxf86vm
	x11-libs/libX11"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Human Resource Machine"
GOG_NATIVE_BIN_BASE="HumanResourceMachine.bin"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/en/game/human_resource_machine"

SRC_URI="gog_human_resource_machine_${PV}.sh"

src_install() {
	gog-native_src_install
	if ! use "x86" ; then
		rm -r "${D}/opt/${PN}/game/HumanResourceMachine.bin.x86"
		rm -r "${D}/opt/${PN}/game/lib64"
	fi

	if ! use "amd64" ; then
		rm -r "${D}/opt/${PN}/game/HumanResourceMachine.bin.x86_64"
		rm -r "${D}/opt/${PN}/game/lib"
	fi
}

#######################################################################
#
# TODO:
#
#  * IUSE bundled-libs, v zakladu povolenej, kterej povoli kopirovani
#    knihoven ze zipu, pokud ho nekdo zakaze, pridaji se zavislosti
#    ze systemu (pozor na useflagy) a smazou se binarni verze knihoven
#    rozbaleny z archivu (libSDL2-2.0.so.0, ..)
