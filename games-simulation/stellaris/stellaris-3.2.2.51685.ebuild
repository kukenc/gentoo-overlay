# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit check-reqs gog-native

DESCRIPTION="An evolution of the grand strategy genre with space exploration at its core (GOG edition)"
HOMEPAGE="https://www.stellaris.com/"
KEYWORDS="-* amd64 x86"
SLOT="0"
IUSE="horizon-signal"

RDEPEND="
	app-crypt/libmd
	dev-libs/libbsd
	media-libs/libglvnd
	sys-apps/util-linux
	sys-devel/gcc
	sys-libs/glibc
	sys-libs/zlib
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXdmcp
	x11-libs/libX11"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

CHECKREQS_DISK_BUILD="30G"

GOG_NATIVE_GAME_NAME="Stellaris"
GOG_NATIVE_BIN_BASE="stellaris"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/stellaris"

SRC_URI="
	stellaris_${PV//./_}.sh
	horizon-signal? ( stellaris_horizon_signal_${PV//./_}.sh )"

PATCHES=(
	"${FILESDIR}/disable-paradox-launcher.patch"
)
