# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop java-pkg-2

MY_JAR="Mindustry.jar"

DESCRIPTION="A sandbox tower-defense game"
HOMEPAGE="https://mindustrygame.github.io"
SRC_URI="https://github.com/Anuken/Mindustry/releases/download/v${PV}/${MY_JAR} -> ${P}.jar"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND=">=virtual/jre-17:*"

src_unpack() {
	# prevent install /var/lib/portage/distfiles/ symlink
	mkdir -p "${S}"
	cp "${DISTDIR}/${P}.jar" "${S}/${MY_JAR}" || die "Cannot copy downloaded jar."
}

src_install() {
	# see: https://wiki.gentoo.org/wiki/Java_Developer_Guide#Filesystem_layout
	java-pkg_jarinto "/opt/${PN}-${SLOT}/lib/"
	java-pkg_dojar "${MY_JAR}"
	# desktop
	make_desktop_entry "java -jar /opt/${PN}-${SLOT}/lib/${MY_JAR}" "Mindustry"
}
