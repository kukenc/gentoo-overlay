# Copyright 2021 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit autotools desktop

DESCRIPTION="Turn-based sci-fi strategy wargame"
HOMEPAGE="https://github.com/signus-game/signus"

if [[ ${PV} == 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}"
	SRC_URI=""
	EGIT_SUBMODULES=()
else
	SRC_URI="${HOMEPAGE}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
fi

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="debug"
RESTRICT="debug? ( strip )"

RDEPEND="
	media-libs/libsdl2:=
	media-libs/sdl2-image:=[jpeg]
	media-libs/sdl2-mixer:=[midi,mod,vorbis]
	media-libs/sdl2-ttf:="
DEPEND="${RDEPEND}
	media-libs/libpng"

src_prepare() {
	default
	cd signus
	eaclocal
	eautoconf
	eautoheader
	eautomake
	cd ../signus-data
	eaclocal
	eautoconf
	eautomake
	cd ..
}

src_configure() {
	cd signus
	econf $(use_enable debug)
	cd ../signus-data
	econf
	cd ..
}

src_compile() {
	cd signus
	emake
	cd ../signus-data
	emake
	cd ..
}

src_install() {
	cd signus
	emake DESTDIR="${D}" install
	dodoc AUTHORS
	cd ../signus-data
	emake DESTDIR="${D}" install
	cd ..
	domenu "${FILESDIR}/signus.desktop"
}
