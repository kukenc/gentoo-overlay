# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit retrogames

# abandonware
LICENSE="all-rights-reserved"
DESCRIPTION="A real-time strategy Dune II video game."
HOMEPAGE="https://www.retrogames.cz/play_451-DOS.php"
SLOT="2"
SRC_URI="
	https://www.retrogames.cz/dos/dune2.zip -> ${P}.zip
	https://www.retrogames.cz/manualy/DOS/Dune_II_-_Manual_-_PC.pdf"

RETROGAMES_NAME="Dune II: The Building of a Dynasty"
RETROGAMES_IMG_NAME="dune2.img"
RETROGAMES_IMG_SIZE="512,8,2,384"
RETROGAMES_COMMAND="dune2"
RETROGAMES_MAN="Dune_II_-_Manual_-_PC.pdf"
