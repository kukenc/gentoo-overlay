# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Kingdom: New Lands is a sequel to Kingdom: Classic that was developed by Noio and published by Raw Fury (GOG edition)"
HOMEPAGE="https://www.kingdomthegame.com/kingdom-new-lands"
KEYWORDS="-* amd64 x86"
SLOT="0"

RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Kingdom: New Lands"
GOG_NATIVE_BIN_BASE="Kingdom"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/kingdom_new_lands"

SRC_URI="kingdom_new_lands_en_${PV//./_}.sh"
