# gentoo-overlay

This is my current Gentoo overlay. I intend to maintain ebuild for packages
that I personaly use.

If you want to help, feel free to contact me :-)

All commits shall be signed by my PGP key.

## Usefull links

* [Functions](https://devmanual.gentoo.org/ebuild-writing/functions/)
* [Variables](https://devmanual.gentoo.org/ebuild-writing/variables/index.html)
* [Install Functions Reference](https://devmanual.gentoo.org/function-reference/install-functions/index.html)
* [Configuring a Package](https://devmanual.gentoo.org/ebuild-writing/functions/src_configure/configuring/index.html)
* [Ebuild](https://devmanual.gentoo.org/eclass-reference/ebuild/)
* [Eclass](https://devmanual.gentoo.org/eclass-writing/)
* [Eclass list](https://devmanual.gentoo.org/eclass-reference/index.html)
* [Ebuild file naming & other rules](https://devmanual.gentoo.org/ebuild-writing/file-format/index.html)

## Factorio

Factorio server ebuild is taken from
[tastytea](https://data.gpo.zugaina.org/tastytea/games-server/factorio-server/).
