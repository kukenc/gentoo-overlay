# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Open-ended programming game. (GOG edition)"
HOMEPAGE="https://zachtronics.com/tis-100/"
KEYWORDS="-* amd64 x86"
SLOT="0"

RDEPEND="
	app-crypt/libmd
	dev-libs/libbsd
	media-libs/libglvnd
	sys-devel/gcc
	sys-libs/glibc
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXcursor
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libX11"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="TIS-100"
GOG_NATIVE_BIN_BASE="tis100"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/en/game/tis100"

VERSION_PV="${PV#*.*.*.}"
TMP_DAY_PV="${PV%*.$VERSION_PV}"
DAY_PV="${TMP_DAY_PV#*.*.}"
TMP_MONTH_PV="${TMP_DAY_PV%*.$DAY_PV}"
MONTH_PV="${TMP_MONTH_PV#*.}"
YEAR_PV="${PV%*.*.*.$VERSION_PV}"
SRC_URI="tis_100_${MONTH_PV}_${DAY_PV}_${YEAR_PV}_${VERSION_PV}.sh"

src_install() {
	gog-native_src_install

	if ! use "x86" ; then
		rm -r "${D}/opt/${PN}/game/tis100.x86"
		rm -r "${D}/opt/${PN}/game/tis100_Data/Mono/x86"
	fi

	if ! use "amd64" ; then
		rm -r "${D}/opt/${PN}/game/tis100.x86_64"
		rm -r "${D}/opt/${PN}/game/tis100_Data/Mono/x86_64"
	fi

	make_desktop_entry "xdg-open \"${GOG_NATIVE_DESTINATION}/game/TIS-100 Reference Manual.pdf\"" "${GOG_NATIVE_GAME_NAME} Manual" "${PN}"
}
