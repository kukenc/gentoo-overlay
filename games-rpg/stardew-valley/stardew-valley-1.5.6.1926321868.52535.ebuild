# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Stardew Valley is an open-ended country-life RPG (GOG edition)"
HOMEPAGE="https://www.stardewvalley.net"
KEYWORDS="-* amd64"
SLOT="0"

RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Stardew Valley"
GOG_NATIVE_BIN_BASE="Stardew Valley"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/stardew_valley"

SRC_URI="stardew_valley_${PV//./_}.sh"

src_install() {
	gog-native_src_install

	exeinto "/opt/${PN}/game"
	doexe "data/noarch/game/StardewValley"
}
