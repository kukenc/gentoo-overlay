# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

#inherit eutils

DESCRIPTION="Tool to hold simple git flow"
HOMEPAGE="https://github.com/kukenc/omgf"

if [[ ${PV} == 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}"
	SRC_URI=""
	EGIT_SUBMODULES=()
	EGIT_BRANCH="dev"
else
	SRC_URI="${HOMEPAGE}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
fi

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~arm64 ~x86"
IUSE=""

BDEPEND="dev-python/docutils"
RDEPEND="
	app-shells/bash
	dev-vcs/git
	sys-apps/coreutils
	sys-apps/gawk
	sys-apps/grep
	sys-apps/sed
	sys-apps/util-linux
	sys-libs/ncurses"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

PATCHES=(
	"${FILESDIR}/000-gentoo-src_install-compatibility.patch"
)

src_install() {
	# see work/${P}/compiled/install
	dir="compiled"
	doman "${dir}/omgf.1"
	dobin "${dir}/omgf"
	insinto "/usr/share/${PN}"
	doins "${dir}/omgf.usage" "${dir}/VERSION"
	# instal my own default cfg
	insinto /etc
	newins "${FILESDIR}/etc_omgf" omgf
}
