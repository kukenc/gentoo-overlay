# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="A puzzle simulation video game (GOG edition)"
HOMEPAGE="https://papersplea.se"
KEYWORDS="-* amd64"
SLOT="0"
IUSE="cz"

RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Papers Please"
GOG_NATIVE_BIN_BASE="PapersPlease"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/papers_please"

SRC_URI="papers_please_${PV//./_}.sh"

src_install() {
	gog-native_src_install

	if use "cz"; then
		# source of cz.zip is:
		#  https://spykertym.cz/papers-please
		# found by
		#  https://prekladyher.eu/preklady/papers-please.1190/
		insinto "/opt/${PN}/game/loc"
		doins "${FILESDIR}/cz.zip"
	fi
}
