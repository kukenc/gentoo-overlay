# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

DESCRIPTION="PMC tool for testing MDG server"
SRC_URI="MDGB-RELEASE-portable-AnyCPU-v${PV}.zip"

SLOT="0"
KEYWORDS="amd64 x86"

RESTRICT="fetch bindist strip"

RDEPEND="dev-lang/mono-basic"

S="${WORKDIR}"

pkg_nofetch() {
	einfo "Please login to PMC local wiki and download ${A} from"
	einfo "MDG project page and move it to your DISTDIR"
}

src_install() {
	insinto "/usr/share/${PN}"
	doins *
}
