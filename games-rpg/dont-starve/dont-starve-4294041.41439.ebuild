# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Uncompromising wilderness survival game full of science and magic (GOG edition)"
HOMEPAGE="https://www.klei.com/games/dont-starve"
KEYWORDS="-* amd64"
SLOT="0"
IUSE="reign-of-giants shipwrecked"

RDEPEND="
	app-crypt/libmd
	dev-libs/gmp
	dev-libs/libbsd
	dev-libs/libtasn1
	dev-libs/libunistring
	dev-libs/nettle
	dev-libs/openssl
	media-libs/libglvnd
	net-dns/libidn2
	net-libs/gnutls
	net-libs/libssh2
	net-libs/nghttp2
	net-misc/curl[gnutls]
	sys-devel/gcc
	sys-libs/glibc
	sys-libs/zlib
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXdmcp
	x11-libs/libX11"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Don't Starve"
GOG_NATIVE_BIN_BASE="dontstarve"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/dont_starve"

SRC_URI="
	don_t_starve_${PV//./_}.sh
	reign-of-giants? ( don_t_starve_reign_of_giants_dlc_${PV//./_}.sh )
	shipwrecked? ( don_t_starve_shipwrecked_dlc_${PV//./_}.sh )"

src_install() {
	GOG_NATIVE_DESTINATION="/opt/${PN}"

	# files
	insinto "${GOG_NATIVE_DESTINATION}"
	doins -r "data/noarch/game" "data/noarch/gameinfo"

	insinto "${GOG_NATIVE_DESTINATION}/support"
	doins "data/noarch/support/gog"*

	# docs
	dodoc -r data/noarch/docs/*

	# icon
	newicon -s 256 "data/noarch/support/icon.png" "${PN}.png"

	# start.sh
	exeinto "${GOG_NATIVE_DESTINATION}"
	doexe "data/noarch/start.sh"

	# runnable
	# This package cannot be install by gog-native_src_install because
	# binaries are in "${GOG_NATIVE_DESTINATION}/game/bin" instead of
	# "${GOG_NATIVE_DESTINATION}/game"
	exeinto "${GOG_NATIVE_DESTINATION}/game/bin"
	doexe "data/noarch/game/bin/${GOG_NATIVE_BIN_BASE}"
	doexe "data/noarch/game/bin/${GOG_NATIVE_BIN_BASE}.sh"

	# desktop
	make_desktop_entry "${GOG_NATIVE_DESTINATION}/start.sh" "${GOG_NATIVE_GAME_NAME}" "${PN}"

	# * QA Notice: Unresolved soname dependencies:
	# *
	# * 	/opt/dont-starve/game/bin/dontstarve: libcurl-gnutls.so.4
	# *
	# This symlink fixed QA Notice for runtime
	dosym "${EPREFIX}/usr/lib64/libcurl.so.4" "${GOG_NATIVE_DESTINATION}/game/bin/lib64/libcurl-gnutls.so.4"
}

#######################################################################
#
# TODO:
#
#    IUSE bundled-libs, v zakladu povolenej, kterej povoli kopirovani
#    knihoven ze zipu, pokud ho nekdo zakaze, pridaji se zavislosti
#    ze systemu (pozor na useflagy) a smazou se binarni verze knihoven
#    rozbaleny z archivu - pozor na libcurl.so.4 fix
#
#    Pokud budou povoleny bundled-libs, asi by stalo zato stejne
#    soubory nasymlinkovat - jako se to dela v systemu, napr:
#    libfmodex64.so -> libfmodex64-4.44.64.so
#
# kukenc@dell /opt/dont-starve/game/bin % ldd dontstarve
# ./dontstarve: ./lib64/libcurl-gnutls.so.4: no version information available (required by ./dontstarve)
# 	linux-vdso.so.1 (0x00007ffc793b8000)
# 	libGL.so.1 => /usr/lib64/libGL.so.1 (0x00007f59ca44d000)
# 	libSDL2-2.0.so.0 => ./lib64/libSDL2-2.0.so.0 (0x00007f59ca35a000)
# 	libcurl-gnutls.so.4 => ./lib64/libcurl-gnutls.so.4 (0x00007f59ca2c2000)
# 	librt.so.1 => /lib64/librt.so.1 (0x00007f59ca2b8000)
# 	libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f59ca298000)
# 	libfmodevent64.so => ./lib64/libfmodevent64.so (0x00007f59ca00b000)
# 	libfmodex64.so => ./lib64/libfmodex64.so (0x00007f59c9c5a000)
# 	libstdc++.so.6 => /usr/lib/gcc/x86_64-pc-linux-gnu/11.2.0/libstdc++.so.6 (0x00007f59c99ba000)
# 	libm.so.6 => /lib64/libm.so.6 (0x00007f59c9888000)
# 	libgcc_s.so.1 => /usr/lib/gcc/x86_64-pc-linux-gnu/11.2.0/libgcc_s.so.1 (0x00007f59c986d000)
# 	libc.so.6 => /lib64/libc.so.6 (0x00007f59c96b1000)
# 	libGLdispatch.so.0 => /usr/lib64/libGLdispatch.so.0 (0x00007f59c95f9000)
# 	libGLX.so.0 => /usr/lib64/libGLX.so.0 (0x00007f59c95c3000)
# 	libdl.so.2 => /lib64/libdl.so.2 (0x00007f59c95bd000)
# 	libnghttp2.so.14 => /usr/lib64/libnghttp2.so.14 (0x00007f59c9591000)
# 	libidn2.so.0 => /usr/lib64/libidn2.so.0 (0x00007f59c956e000)
# 	libssh2.so.1 => /usr/lib64/libssh2.so.1 (0x00007f59c952a000)
# 	libnettle.so.8 => /usr/lib64/libnettle.so.8 (0x00007f59c94e3000)
# 	libgnutls.so.30 => /usr/lib64/libgnutls.so.30 (0x00007f59c92f5000)
# 	libssl.so.1.1 => /usr/lib64/libssl.so.1.1 (0x00007f59c9263000)
# 	libcrypto.so.1.1 => /usr/lib64/libcrypto.so.1.1 (0x00007f59c8fa5000)
# 	libz.so.1 => /lib64/libz.so.1 (0x00007f59c8f8c000)
# 	/lib64/ld-linux-x86-64.so.2 (0x00007f59ca4f1000)
# 	libfmodex64-4.44.64.so => ./lib64/libfmodex64-4.44.64.so (0x00007f59c8bdb000)
# 	libX11.so.6 => /usr/lib64/libX11.so.6 (0x00007f59c8a97000)
# 	libunistring.so.2 => /usr/lib64/libunistring.so.2 (0x00007f59c8914000)
# 	libtasn1.so.6 => /usr/lib64/libtasn1.so.6 (0x00007f59c88fe000)
# 	libhogweed.so.6 => /usr/lib64/libhogweed.so.6 (0x00007f59c88b4000)
# 	libgmp.so.10 => /usr/lib64/libgmp.so.10 (0x00007f59c8838000)
# 	libxcb.so.1 => /usr/lib64/libxcb.so.1 (0x00007f59c880b000)
# 	libXau.so.6 => /usr/lib64/libXau.so.6 (0x00007f59c8806000)
# 	libXdmcp.so.6 => /usr/lib64/libXdmcp.so.6 (0x00007f59c87fe000)
# 	libbsd.so.0 => /usr/lib64/libbsd.so.0 (0x00007f59c87e7000)
# 	libmd.so.0 => /usr/lib64/libmd.so.0 (0x00007f59c87d9000
#
# kukenc@dell /opt/dont-starve/game/bin % ldd dontstarve | grep '=' | cut -d= -f1 | xargs -n1 equery b
#  * Searching for libGL.so.1 ...
# media-libs/libglvnd-1.4.0 (/usr/lib/libGL.so.1 -> libGL.so.1.7.0)
# media-libs/libglvnd-1.4.0 (/usr/lib64/libGL.so.1 -> libGL.so.1.7.0)
#  * Searching for libSDL2-2.0.so.0 ...
# media-libs/libsdl2-2.0.16-r1 (/usr/lib/libSDL2-2.0.so.0 -> libSDL2-2.0.so.0.16.0)
# media-libs/libsdl2-2.0.16-r1 (/usr/lib64/libSDL2-2.0.so.0 -> libSDL2-2.0.so.0.16.0)
#  * Searching for libcurl-gnutls.so.4 ...
#  * Searching for librt.so.1 ...
# sys-libs/glibc-2.33-r7 (/lib/librt.so.1 -> librt-2.33.so)
# sys-libs/glibc-2.33-r7 (/lib64/librt.so.1 -> librt-2.33.so)
#  * Searching for libpthread.so.0 ...
# sys-libs/glibc-2.33-r7 (/lib/libpthread.so.0 -> libpthread-2.33.so)
# sys-libs/glibc-2.33-r7 (/lib64/libpthread.so.0 -> libpthread-2.33.so)
#  * Searching for libfmodevent64.so ...
#  * Searching for libfmodex64.so ...
#  * Searching for libstdc++.so.6 ...
# sys-devel/gcc-11.2.0 (/usr/lib/gcc/x86_64-pc-linux-gnu/11.2.0/32/libstdc++.so.6 -> libstdc++.so.6.0.29)
# sys-devel/gcc-11.2.0 (/usr/lib/gcc/x86_64-pc-linux-gnu/11.2.0/libstdc++.so.6 -> libstdc++.so.6.0.29)
#  * Searching for libm.so.6 ...
# sys-libs/glibc-2.33-r7 (/lib/libm.so.6 -> libm-2.33.so)
# sys-libs/glibc-2.33-r7 (/lib64/libm.so.6 -> libm-2.33.so)
#  * Searching for libgcc_s.so.1 ...
# sys-devel/gcc-11.2.0 (/usr/lib/gcc/x86_64-pc-linux-gnu/11.2.0/32/libgcc_s.so.1)
# sys-devel/gcc-11.2.0 (/usr/lib/gcc/x86_64-pc-linux-gnu/11.2.0/libgcc_s.so.1)
#  * Searching for libc.so.6 ...
# sys-libs/glibc-2.33-r7 (/lib/libc.so.6 -> libc-2.33.so)
# sys-libs/glibc-2.33-r7 (/lib64/libc.so.6 -> libc-2.33.so)
#  * Searching for libGLdispatch.so.0 ...
# media-libs/libglvnd-1.4.0 (/usr/lib/libGLdispatch.so.0 -> libGLdispatch.so.0.0.0)
# media-libs/libglvnd-1.4.0 (/usr/lib64/libGLdispatch.so.0 -> libGLdispatch.so.0.0.0)
#  * Searching for libGLX.so.0 ...
# media-libs/libglvnd-1.4.0 (/usr/lib/libGLX.so.0 -> libGLX.so.0.0.0)
# media-libs/libglvnd-1.4.0 (/usr/lib64/libGLX.so.0 -> libGLX.so.0.0.0)
#  * Searching for libdl.so.2 ...
# sys-libs/glibc-2.33-r7 (/lib/libdl.so.2 -> libdl-2.33.so)
# sys-libs/glibc-2.33-r7 (/lib64/libdl.so.2 -> libdl-2.33.so)
#  * Searching for libfmodex64-4.44.64.so ...
#  * Searching for libX11.so.6 ...
# x11-libs/libX11-1.7.3 (/usr/lib/libX11.so.6 -> libX11.so.6.4.0)
# x11-libs/libX11-1.7.3 (/usr/lib64/libX11.so.6 -> libX11.so.6.4.0)
#  * Searching for libxcb.so.1 ...
# x11-libs/libxcb-1.14 (/usr/lib/libxcb.so.1 -> libxcb.so.1.1.0)
# x11-libs/libxcb-1.14 (/usr/lib64/libxcb.so.1 -> libxcb.so.1.1.0)
#  * Searching for libXau.so.6 ...
# x11-libs/libXau-1.0.9-r1 (/usr/lib/libXau.so.6 -> libXau.so.6.0.0)
# x11-libs/libXau-1.0.9-r1 (/usr/lib64/libXau.so.6 -> libXau.so.6.0.0)
#  * Searching for libXdmcp.so.6 ...
# x11-libs/libXdmcp-1.1.3 (/usr/lib/libXdmcp.so.6 -> libXdmcp.so.6.0.0)
# x11-libs/libXdmcp-1.1.3 (/usr/lib64/libXdmcp.so.6 -> libXdmcp.so.6.0.0)
#  * Searching for libbsd.so.0 ...
# dev-libs/libbsd-0.11.3 (/usr/lib/libbsd.so.0 -> libbsd.so.0.11.3)
# dev-libs/libbsd-0.11.3 (/usr/lib64/libbsd.so.0 -> libbsd.so.0.11.3)
#  * Searching for libmd.so.0 ...
# app-crypt/libmd-1.0.4 (/usr/lib/libmd.so.0 -> libmd.so.0.0.5)
# app-crypt/libmd-1.0.4 (/usr/lib64/libmd.so.0 -> libmd.so.0.0.5)
