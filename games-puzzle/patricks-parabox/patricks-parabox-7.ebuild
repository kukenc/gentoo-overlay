# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

DESCRIPTION="Explore a unique recursive system of boxes within boxes within boxes within boxes. (itch.io edition)."
HOMEPAGE="https://www.patricksparabox.com"

inherit desktop xdg-utils

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="-* amd64 x86"
RESTRICT="fetch bindist strip"

# ldd /opt/patricks-parabox/patricks-parabox | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
# ldd /opt/patricks-parabox/UnityPlayer.so | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}"

SRC_URI="patricks-parabox-linux_${PV}.zip"

S="${WORKDIR}/Patrick's Parabox"

DOCS="readme.txt"

pkg_nofetch() {
	einfo "Please buy and download ${A} from:"
	einfo "   https://patricktraynor.itch.io/patricks-parabox"
	einfo "and move it to your DISTDIR"
}

src_install() {
	insinto "/opt/${PN}"
	#doins -r "Patrick's Parabox_Data" "extras"
	doins -r "Patrick's Parabox_Data"
	doins UnityPlayer.so

	einstalldocs

	exeinto "/opt/${PN}"
	doexe "Patrick's Parabox.x86_64"

	# desktop
	dosym "Patrick's Parabox.x86_64" "/opt/${PN}/patricks-parabox"
	make_desktop_entry "/opt/${PN}/patricks-parabox" "Patrick's Parabox" "${PN}"
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

gog-native_pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
