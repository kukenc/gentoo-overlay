# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit xdg-utils

# abandonware
LICENSE="all-rights-reserved"
DESCRIPTION="Ski or Die is a 1990 winter sports game."
HOMEPAGE="https://www.oldgames.sk/en/game/ski-or-die/"
SLOT="0"
SRC_URI="https://www.starehry.eu/download/sport/Ski.or.Die.zip -> ${P}.zip"

KEYWORDS="amd64 arm ppc ppc64 x86"
RESTRICT="mirror test"
BDEPEND="app-arch/unzip"
RDEPEND="games-emulation/dosbox"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

NAME="Ski or Die"
COMMAND="S.EXE"

S="${WORKDIR}/skiordie"

src_install() {
	DESTINATION="/opt/${PN}"

	# files
	insinto "${DESTINATION}"
	doins *

	# desktop
	DESKTOP="${T}/${PN}.desktop"
	cat <<-EOF > "${DESKTOP}" || die
	[Desktop Entry]
	Name=${NAME}
	Type=Application
	Comment=${DESCRIPTION}
	Exec=dosbox "${DESTINATION}/${COMMAND}"
	Icon=/usr/share/pixmaps/dosbox.ico
	Categories=Game;
	EOF
	(
		insopts -m "0644"
		insinto "/usr/share/applications"
		doins "${DESKTOP}"
	) || die "installing desktop file failed"
}

retrogames_pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

retrogames_pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
