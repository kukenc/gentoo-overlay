# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Tool created for effortless and automated connection to CheckPoint's SNX VPN."
HOMEPAGE="https://gitlab.com/Xoores/snxwrapper"

if [[ ${PV} == 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}"
	SRC_URI=""
	EGIT_SUBMODULES=()
	EGIT_BRANCH="dev"
else
	SRC_URI="${HOMEPAGE}/-/archive/${PV}/${P}.tar.gz"
fi

KEYWORDS="amd64 x86"

LICENSE="Unlicense"
SLOT="0"

DEPEND="
	dev-lang/python
	dev-python/aiohttp
	dev-python/async-timeout
	dev-python/beautifulsoup4
	dev-python/lxml
	dev-python/pycryptodome
	dev-python/pyopenssl
	dev-python/rsa
	net-vpn/snx"
RDEPEND="${DEPEND}"

DOCS="
	CHANGELOG.md
	LICENSE
	README.md"

src_install() {
	# script
	dobin "${PN}"
	# service
	newinitd "${FILESDIR}/checkpoint-vpn-client.initd" checkpoint-vpn-client
	newconfd "${FILESDIR}/checkpoint-vpn-client.confd" checkpoint-vpn-client
	# doc
	einstalldocs
	# configuration example
	insinto "/usr/share/${PN}"
	newins "${FILESDIR}/${PN}-${PV}.config" "${PN}.config"
}

pkg_postinst() {
	einfo ""
	einfo "Copy example config file to your home directory"
	einfo "  cp \"/usr/share/${PN}/${PN}.config\" \"\${HOME}/.config/${PN}/config\""
	einfo "and fill you connection information."
	einfo ""
}
