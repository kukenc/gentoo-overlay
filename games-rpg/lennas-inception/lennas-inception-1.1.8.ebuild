# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

DESCRIPTION="Explore a glitched-out kingdom in this top-down Zelda-like action-adventure game. (itch.io edition)."
HOMEPAGE="https://lennasinception.com"

inherit desktop xdg-utils

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="-* amd64"
RESTRICT="fetch bindist strip"

# ldd /opt/lennas-inception/jre/bin/java| grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
# ldd /opt/lennas-inception/jre/lib/amd64/jli/libjli.so | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
RDEPEND="
	sys-libs/glibc
	sys-libs/zlib"
DEPEND="
	${RDEPEND}"

SRC_URI="lennas-inception-linux-amd64_${PV}.zip"

S="${WORKDIR}/"

DOCS="
	COPYRIGHT.txt
	README.txt"

pkg_nofetch() {
	einfo "Please buy and download ${A} from:"
	einfo "   https://tccoxon.itch.io/lennas-inception"
	einfo "and move it to your DISTDIR"
}

src_install() {
	insinto "/opt/${PN}"

	# TODO: useflag pro systemovou javu (pouzije se tak ze se nezkopije slozka jre ze zipu)
	doins -r assets jre lib
	doins "${FILESDIR}/launch-config.json"

	einstalldocs

	exeinto "/opt/${PN}"
	doexe lennasinception

	# TODO: useflag pro systemovou javu (pouzije se tak ze se nezkopije slozka jre ze zipu)
	exeinto "/opt/${PN}/jre/bin/"
	doexe jre/bin/java

	# desktop
	make_desktop_entry "/opt/${PN}/lennasinception" "Lenna's inception" "${PN}"
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

gog-native_pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
