# Copyright 2021 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

DESCRIPTION="CheckPoint SSL Network Extender"
HOMEPAGE="https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doGoviewsolutiondetails=&solutionid=sk65210"

LICENSE="checkpoint-EULA"
SLOT="0"
KEYWORDS="~amd64 ~x86"
RESTRICT="fetch bindist strip"

BDEPEND="
	app-arch/bzip2
	sys-apps/grep
	app-arch/tar
	sys-apps/coreutils"
RDEPEND="sys-libs/libstdc++-v3"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

SRC_URI="snx_install-${PV}.sh"

QA_PREBUILT="usr/bin/snx"

S="${WORKDIR}"

pkg_nofetch() {
	einfo "Please download ${A} from:"
	einfo "  https://supportcenter.checkpoint.com/supportcenter/portal/user/anon/page/default.psml/media-type/html?action=portlets.DCFileAction&eventSubmit_doGetdcdetails=&fileid=22824"
	einfo "or"
	einfo "  https://cpfw.example.com/SNX/INSTALL/snx_install.sh"
	einfo "and move it to your DISTDIR"
}

src_unpack() {
	archive_offset="$(grep --binary-files=text --max-count=1 ARCHIVE_OFFSET "${DISTDIR}/${A}" | cut -d= -f2)"
	tail -n "+${archive_offset}" "${DISTDIR}/${A}" | bunzip2 -c - | tar xf - snx > /dev/null 2>&1
}

src_install() {
	dobin snx
	fperms u=rxs,g=x,o=x "/usr/bin/snx"
	diropts "--owner=root --group=root --mode=u=rwx"
	keepdir /etc/snx
	keepdir /etc/snx/tmp
	newinitd "${FILESDIR}/${PN}d.initd" "${PN}d"
	newconfd "${FILESDIR}/${PN}d.confd" "${PN}d"
}

pkg_prerm() {
	# original uninstall procedure
	# /usr/bin/snx -d > /dev/null
	# rm -f /usr/bin/snx
	# rm -f /usr/bin/snx_uninstall
	# rm -f /usr/lib/libcpc++-libc6.1-2.so.3
	# rm -rf /etc/snx
	# disconnect snx
	/usr/bin/snx -d > /dev/null
}
