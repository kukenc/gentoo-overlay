# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Kingdom: Classic is a 2D sidescrolling strategy/resource management with beautiful, modern pixel art aesthetic (GOG edition)"
HOMEPAGE="https://www.kingdomthegame.com/kingdom-classic"
KEYWORDS="-* amd64 x86"
SLOT="0"

RDEPEND="
	app-crypt/libmd
	dev-libs/libbsd
	media-libs/libglvnd
	sys-devel/gcc
	sys-libs/glibc
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXcursor
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libX11"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Kingdom: Classic"
GOG_NATIVE_BIN_BASE="Kingdom"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/kingdom_new_lands"

SRC_URI="gog_kingdom_${PV}.sh"

src_install() {
	gog-native_src_install

	if ! use x86 ; then
		# QA_EXECSTACK="
		# 	opt/kingdom-classic/game/Kingdom.x86
		# 	opt/kingdom-classic/game/Kingdom_Data/Mono/x86/libmono.so"
		rm "${D}/opt/${PN}/game/Kingdom.x86"
		rm -r "${D}/opt/${PN}/game/Kingdom_Data/Mono/x86"
	fi
}
