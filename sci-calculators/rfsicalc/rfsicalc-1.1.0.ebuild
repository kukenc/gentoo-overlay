# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

DESCRIPTION="PMC tool calculate old style RFSI"

inherit desktop

SRC_URI="${PN}-v${PV}.zip"

SLOT="0"
KEYWORDS="amd64 x86"

RESTRICT="fetch bindist strip"

RDEPEND="dev-lang/mono"

S="${WORKDIR}"

pkg_nofetch() {
	einfo "Please login to PMC local git and download ${A} from"
	einfo "project release page and move it to your DISTDIR"
}

src_install() {
	insinto "/usr/share/${PN}"
	doins *
	domenu "${FILESDIR}/${PN}.desktop"
}
