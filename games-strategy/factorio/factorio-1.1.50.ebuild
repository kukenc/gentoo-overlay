# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

DESCRIPTION="Factorio is a construction and management simulation game. Kickstarter version."
HOMEPAGE="https://factorio.com"

inherit desktop

LICENSE="factorio-EULA"
SLOT="0"
KEYWORDS="amd64 x86"
RESTRICT="fetch bindist strip"

# ldd /usr/bin/factorio | grep '=' | cut -d' ' -f3 | xargs -n1 -P $(nproc) equery b | sort | uniq
RDEPEND="
	app-crypt/libmd
	dev-libs/libbsd
	media-libs/alsa-lib
	media-libs/flac
	media-libs/libglvnd
	media-libs/libogg
	media-libs/libsndfile
	media-libs/libvorbis
	media-libs/opus
	media-sound/pulseaudio
	sys-apps/dbus
	sys-apps/util-linux
	sys-libs/glibc
	x11-libs/libICE
	x11-libs/libSM
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXcursor
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXinerama
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-libs/libX11"
DEPEND="
	${RDEPEND}"

SRC_URI="factorio_alpha_x64_${PV}.tar.xz"

S="${WORKDIR}/factorio"

DOCS="
	data/credits.txt
	data/eula.txt
	data/changelog.txt
	data/licenses.txt"

HTML_DOCS="doc-html"

QA_PREBUILT="factorio"

pkg_nofetch() {
	einfo "Please buy and download ${A} from:"
	einfo "   https://factorio.com/get-download/${PV}/alpha/linux64"
	einfo "and move it to your DISTDIR"
}

src_install() {
	insinto "/usr/share/${PN}"
	doins -r data/base data/core
	einstalldocs
	dobin bin/x64/factorio
	domenu "${FILESDIR}/factorio.desktop"
}
