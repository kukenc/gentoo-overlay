# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# @ECLASS: gog-dosbox.eclass
# @MAINTAINER:
# Martin Kužel <martin@kuzelovi.eu>
# @SUPPORTED_EAPIS: 7 8
# @BLURB: support for GOG.com linux games over dosbox installer


case ${EAPI} in
	7|8) ;;
	*) die "${ECLASS}: EAPI ${EAPI:-0} not supported" ;;
esac


inherit desktop xdg-utils


LICENSE="gog-EULA"
RESTRICT="bindist fetch mirror test"
BDEPEND="app-arch/unzip"
RDEPEND="games-emulation/dosbox"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

EXPORT_FUNCTIONS pkg_nofetch src_unpack src_prepare src_install pkg_postinst pkg_postrm


gog-dosbox_pkg_nofetch() {
	einfo "Please buy and download:"
	for GOG_DIST_FILE in ${A} ; do
		einfo "  ${GOG_DIST_FILE}"
	done
	einfo "from:"
	einfo "  ${GOG_BUY_PAGE}"
	einfo "and move it to your DISTDIR"
}


gog-dosbox_src_unpack() {
	for GOG_DIST_FILE in ${A} ; do
		echo ">>> Unpacking ${GOG_DIST_FILE} to ${S}"
		unzip "${DISTDIR}/${GOG_DIST_FILE}" -d "${S}" > /dev/null 2>&1 || true
	done
}


gog-dosbox_src_prepare() {
	# for default see https://devmanual.gentoo.org/ebuild-writing/functions/src_prepare/index.html
	if [[ $(declare -p PATCHES 2>/dev/null) == "declare -a"* ]]; then
		[[ -n ${PATCHES[@]} ]] && eapply "${PATCHES[@]}"
	else
		[[ -n ${PATCHES} ]] && eapply ${PATCHES}
	fi

	sed -i -e 's,./dosbox/dosbox,dosbox,g' "data/noarch/support/gog_com.shlib" || \
		die "Cannot patch gog_com.shlib to use local dosbox instance"

	eapply_user
}


gog-dosbox_src_install() {
	GOG_DESTINATION="/opt/${PN}"

	# files
	insinto "${GOG_DESTINATION}"
	doins -r "data/noarch/data" "data/noarch/gameinfo" "data/noarch/dosbox"*".conf"

	insinto "${GOG_DESTINATION}/support"
	doins "data/noarch/support/gog"*

	# docs
	dodoc -r data/noarch/docs/*
	# QA Notice: One or more compressed files were found in docompress-ed
	# directories. Please fix the ebuild not to install compressed files
	# (manpages, documentation) when automatic compression is used:
	rm "${D}/usr/share/doc/${P}/dosbox-"*

	# icon
	newicon -s 256 "data/noarch/support/icon.png" "${PN}.png"

	# start.sh
	exeinto "${GOG_DESTINATION}"
	doexe "data/noarch/start.sh"

	# desktop
	make_desktop_entry "${GOG_DESTINATION}/start.sh" "${GOG_GAME_NAME}" "${PN}"
}


gog-dosbox_pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}


gog-dosbox_pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
