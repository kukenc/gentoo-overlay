# Copyright 2022 Martin Kuzel
# Distributed under the terms of the GNU General Public License v3

EAPI=8

inherit gog-native

DESCRIPTION="Build railways, manage traffic and stay accident-free. (GOG edition)"
HOMEPAGE="http://train-valley.com/tv1.html"
KEYWORDS="-* amd64 x86"
SLOT="0"

RDEPEND="
	sys-devel/gcc
	sys-libs/glibc"
DEPEND="
	${RDEPEND}
	${BDEPEND}"

GOG_NATIVE_GAME_NAME="Train Valley"
GOG_NATIVE_BIN_BASE="train-valley"
GOG_NATIVE_BUY_PAGE="https://www.gog.com/game/Train_Valley"

SRC_URI="train_valley_${PV//./_}.sh"

src_install() {
	gog-native_src_install

	if ! use "x86" ; then
		rm -r "${D}/opt/${PN}/game/train-valley_Data/Mono/x86"
		rm -r "${D}/opt/${PN}/game/train-valley_Data/Plugins/x86"
	fi

	if ! use "amd64" ; then
		rm -r "${D}/opt/${PN}/game/lib64"
	fi
}
